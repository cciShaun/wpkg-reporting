﻿<#	
	.NOTES
	===========================================================================
	 Created with: 	SAPIEN Technologies, Inc., PowerShell Studio 2017 v5.4.138
	 Created on:   	1/26/2018 3:10 PM
	 Created by:   	SSandler
	 Organization: 	
	 Filename:     	Host.ps1
	===========================================================================
	.DESCRIPTION
		A description of the file.
#>


#region Software
class Software {
	[string]$Name
	[string]$ID = ""
	[bool]$Installed = $false
	[string]$Revision = '0'
	[bool]$check = $false
	
	static [Software] NewSoftware ([string]$name)
	{
		$tmp = New-Object Software
		$tmp.Name = $name
		return $tmp
	}
	static [Software] NewSoftware ([string]$name, [string]$id)
	{
		$tmp = New-Object Software
		$tmp.Name = $name
		$tmp.ID = $id
		
		return $tmp
	}
	static [Software] NewSoftware ([string]$name, [bool]$flag)
	{
		$tmp = New-Object Software
		$tmp.Name = $name
		$tmp.Installed = $flag
		
		return $tmp
	}
	static [Software] NewSoftware ([string]$name, [string]$id, [bool]$flag)
	{
		$tmp = New-Object Software
		$tmp.Name = $name
		$tmp.ID = $id
		$tmp.Installed = $flag
		
		return $tmp
	}
	static [Software] NewSoftware ([string]$name, [string]$id, [string]$rev)
	{
		$tmp = New-Object Software
		$tmp.Name = $name
		$tmp.ID = $id
		$tmp.Revision = $rev
		
		return $tmp
	}
	static [Software] NewSoftware ([string]$name, [string]$id, [string]$rev, [bool]$checkFlag)
	{
		$tmp = New-Object Software
		$tmp.Name = $name
		$tmp.ID = $id
		$tmp.Revision = $rev
		$tmp.check = $checkFlag
		
		return $tmp
	}
	
	[void] SetSoftwareInstall ([string]$sftID, [bool]$fg)
	{
		if ($sftID -eq $this.ID)
		{
			Write-Host $this.Name
			$this.Installed = $fg	
		}
	}
	
	[string] PrintSoftwareValues ()
	{
		[string]$tmp = "Software Name: " + $this.Name + "`n"
		$tmp += "ID: " + $this.ID + "`n"
		$tmp += "Installed: " + $this.Installed + "`n"
		$tmp += "Revision: " + $this.Revision + "`n"
		
		return $tmp
	}
	
	[bool] HasCheck ()
	{
		return $this.check
	}
}
class SoftwareList {
	[Software[]]$MySoftware = @()
	
	[Software[]] GetSoftware () {
		return $this.MySoftware
	}
	AddSoftware ([Software]$tmp)
	{
		$this.MySoftware += $tmp
	}
	
	[Software] GetSoftwareByID ([string]$id)
	{
		foreach ($package in $this.MySoftware)
		{
			if ($package.ID -eq $id)
			{
				return $package	
			}
		}
		return $null
	}
	
	PrintSoftwareList () {
		foreach ($software in $this.MySoftware)
		{
			Write-Host $software.Name " | " $software.ID " | " $software.Revision
		}
	}
}
#endregion

#region Profile
Class Profile {
	[string]$Name
	[SoftwareList]$ProfileSoftware
	
	static [Profile] NewProfile ([string]$name, [SoftwareList]$software)
	{
		$tmp = New-Object Profile
		$tmp.Name = $name
		$tmp.ProfileSoftware = $software
		
		return $tmp
	}
	
	[void] SetSoftwareInstall ([string]$sftID, [bool]$flg)
	{
		foreach ($sft in $this.ProfileSoftware.MySoftware)
		{
			[Software]$sft = $sft
			$sft.SetSoftwareInstall($sftID, $flg)
		}
	}
	
	[string] ReportProfile ()
	{
		[string]$tmp += $this.Name + "`n"
		$tmp += "Software Name			Installed`n"
		$tmp += "----------------------------------------------`n"
		foreach ($sft in $this.ProfileSoftware.MySoftware)
		{
			$tmp += $sft.Name + "			" + $sft.Installed + "`n"
		}
		
		return $tmp
	}
}
class ProfileList {
	[Profile[]]$MyProfiles = @()
	
	static [ProfileList] NewProfileList ()
	{
		return New-Object ProfileList	
	}
	
	AddProfile ([Profile]$prof)
	{
		$this.MyProfiles += $prof
	}
	[void] AddProfileRange ([Profile[]]$profileArray)
	{
		foreach ($profile in $profileArray)
		{
			$this.MyProfiles += $profile
		}	
	}
	
	[Profile] GetProfileByName ([string]$name)
	{
		foreach ($profile in $this.MyProfiles)
		{
			if ($profile.Name -eq $name)
			{
				return $profile	
			}
		}
		return $null #No Profile By That Name
	}
	
	[void] PrintProfileList ()
	{
		foreach ($profile in $this.MyProfiles)
		{
			$tmp = ""
			foreach ($software in $profile.ProfileSoftware.GetSoftware())
			{
				$tmp += $software.ID + " | "
			}
			Write-Host $profile.Name $tmp
		}	
	}
}
#endregion

#region Host
<#
Host Class is built to track all the software that is installed and what should be installed based on the profiles the host are part of.
#>
Class Host
{
	[string]$Name
	[ProfileList]$MyProfileList = [ProfileList]::NewProfileList()
	[string]$OS
	[string]$IP
	[string]$lastCheckinDate
	[bool]$wpkgCompliant = $false
	[bool]$filterOut = $false
	
	static [Host] NewHost ([string]$name)
	{
		$tmp = New-Object Host
		$tmp.Name = $name
		
		return $tmp
	}
	
	[void] AddProfile ([Profile]$prof)
	{
		$this.MyProfileList.AddProfile($prof)
	}
	
	
	[void] SetSoftwareInstall ([string]$sft, [bool]$flg)
	{
		foreach ($prf in $this.MyProfileList.MyProfiles)
		{
			$prf.SetSoftwareInstall($sft, $flg)
		}
	}
	
	[bool] Compare ([host]$hst1)
	{
		if ($this.Name.ToLower() -gt $hst1.Name.ToLower())
		{
			return $true
		}
		else
		{
			return $false	
		}
	}
	[bool] CompareIP ([Host]$hst)
	{
		if ($this.IP -gt $hst.IP)
		{
			return $true
		}
		else
		{
		return $false	
		}
	}
	
	[string] GetName ()
	{
		return $this.Name
	}
	[string] PrintHostValues()
	{
		[string]$tmp = "Hostname: " + $this.Name + "`n"
		$tmp += "Compliant: " + $this.wpkgCompliant + "`n"
		$tmp += "IP Address: " + $this.IP + "`n"
		$tmp += "OS: " + $this.OS + "`n"
		$tmp += "Last Checkin: " + $this.lastCheckinDate
		return $tmp
	}
	[string] ReportHost ()
	{
		[string]$tmp = "Hostname: " + $this.Name + "`n"
		$tmp += "IP Address: " + $this.IP + "`n"
		$tmp += "OS: " + $this.OS + "`n"
		$tmp += "Last Checkin: " + $this.lastCheckinDate + "`n"
		$tmp += "Compliant: " + $this.wpkgCompliant + "`n"
		$tmp += "Profiles: `n"
		foreach ($prof in $this.MyProfileList.MyProfiles)
		{
			$tmp += $prof.ReportProfile()
			$tmp += "`n`n"
		}
		$tmp += "`n________________________________________________________________________"
		return $tmp
	}
}
class HostList
{
	[Host[]]$MyHosts = @()
	
	AddHost ([Host]$newHost)
	{
		$this.MyHosts += $newHost
	}
	AddHost ([string]$name)
	{
		$tmp = New-Object Host
		$tmp.Name = $name
		$this.AddHost($tmp)
	}
	AddHostRange ([Host[]]$hostArray)
	{
		foreach ($hst in $hostArray)
		{
			[Host]$hst = $hst #cast to Host
			[int]$hstIndex = $this.IndexOf($hst.Name)
			if ($hstIndex -ne -1) #There is a host by this name. Verify if there are new profiles to add to this host
			{
				foreach ($NewPrf in $hst.MyProfileList.MyProfiles)
				{
					[Profile]$NewPrf = $NewPrf
					[bool]$isNewProfile = $true
					foreach ($prf in $this.MyHosts[$hstIndex].MyProfileList.MyProfiles) #check against profiles
					{
						[Profile]$prf = $prf
						if ($NewPrf.Name -eq $prf.Name)
						{
							$isNewProfile = $false
						}
					}
					if ($isNewProfile -eq $true)
					{
						$this.MyHosts[$hstIndex].MyProfileList.AddProfile($NewPrf)
					}
				}
			}
			else
			{
				$this.AddHost($hst)
			}
		}
	}
	
	[Host] GetHostByName ([string]$hostName)
	{
		[Host]$tmp = New-Object Host
		foreach ($item in $this.MyHosts)
		{
			if ($item.Name -eq $hostName)
			{
				$tmp = $item
				return $tmp
				break
			}
		}
		return $null
	}
	[int] IndexOf ([string]$name)
	{
		for ($i=0; $i -lt $this.MyHosts.Count; $i++) {
			if ($this.MyHosts[$i].Name -eq $name)
			{
				return $i	
			}
		}
		return -1
		
	}
	<#
		Simple bubble sort by hostname value.
	#>
	[void] Sort ()
	{
		[bool]$changeFlag = $false
		for ($i=0; $i -lt $this.MyHosts.Length-1; $i++) {
			if ($this.MyHosts[$i].Compare($this.MyHosts[$i + 1])) #returns true if the hostname of the given host is < the value of the $this object
			{
				$this.Swap($i, $i + 1)
				$changeFlag = $true
			}
		}
		
		if ($changeFlag -eq $true)
		{
			$this.Sort()
		}
	}
	
	[void] SortByIP ()
	{
		[bool]$changeFlag = $false
		for ($i = 0; $i -lt $this.MyHosts.Length - 1; $i++)
		{
			if ($this.MyHosts[$i].CompareIP($this.MyHosts[$i + 1])) #returns true if the hostname of the given host is < the value of the $this object
			{
				$this.Swap($i, $i + 1)
				$changeFlag = $true
			}
		}
		
		if ($changeFlag -eq $true)
		{
			$this.SortByIP()
		}
	}
	
	[void] Swap ([int]$index1, [int]$index2)
	{
		[Host]$hstTmp = $this.MyHosts[$index1]
		$this.MyHosts[$index1] = $this.MyHosts[$index2]
		$this.MyHosts[$index2] = $hstTmp
		
		return
	}
	
	[void] FilterBy ([string]$filterWhere, [string]$searchText)
	{
		for ($i = 0; $i -lt $this.MyHosts.Count; $i++)
		{
			[bool]$flag = $false
			[host]$currHost = $this.MyHosts[$i]
			switch ($filterWhere) {
				"Hostname" {
					if ($currHost.Name -like "*" + $searchText + "*")
					{
						$flag = $true
					}
				}
				"Profile" {
					foreach ($prof in $currHost.MyProfileList.MyProfiles)
					{
						if ($prof.Name -like "*" + $searchText + "*")
						{
								$flag = $true
						}	
					}
				}
				"Software" {
					foreach ($prof in $currHost.MyProfileList.MyProfiles)
					{
						foreach ($sft in $prof.ProfileSoftware.MySoftware)
						{
							if ($sft.Name -like "*" + $searchText + "*")
							{
								$flag = $true	
							}	
						}
					}
				}
				default {
					
				}
			}
			if ($flag)
			{
				$this.MyHosts[$i].FilterOut = $false
			}
			else
			{
				$this.MyHosts[$i].FilterOut = $true	
			}
		}
	}
	
	[void] PrintHostList ()
	{
		foreach ($hst in $this.MyHosts)
		{
			[string]$tmp ="HostName: " + $hst.Name + " | Profiles: "
			foreach ($prof in $hst.MyProfileList.MyProfiles)
			{
				$tmp += $prof.Name + " | "
				foreach ($pkg in $prof.ProfileSoftware.GetSoftware())
				{
						$tmp += $pkg.Name + ": " + $pkg.Installed + " - "
				}
			}
			Write-Host ""
			Write-Host $tmp
		}	
	}
}
#endregion